package com.revature.autoquote

object QuotationGenerator
{
    fun getQuotes(vehicle: Vehicle):ArrayList<AQuote>
    {
        var temp: ArrayList<AQuote> = ArrayList<AQuote>()
        temp.add(AQuote("Ford", 15.0, 47.9))
        temp.add(AQuote("Nissan", 150.0, 12.3))
        temp.add(AQuote("Ferrari", 1234.654, 123.654))
        temp.add(AQuote("Bugati", 123.345,12.56))

        return temp
    }
}