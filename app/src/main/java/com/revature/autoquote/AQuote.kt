package com.revature.autoquote

data class AQuote (val company:String="None", val premium:Double = 0.0, val deductible:Double = 0.0){
}